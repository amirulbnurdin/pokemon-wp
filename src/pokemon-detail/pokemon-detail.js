import './pokemon-detail.css';
import logo2 from '../pokeball.svg';
import React from 'react';
import { pokeListStore, resListPoke } from '../store/redux.store';
import Button from 'react-bootstrap/Button';
import { Card, Col, Container, Form, Image, Modal, Row, Toast } from 'react-bootstrap';
import { createStore } from 'redux';

const store = createStore(pokeListStore)
export default class PokemonDetailClass extends React.Component {
    detailPokemon = [];

    constructor(props) {
        super(props);
        this.handleHideToast = this.handleHideToast.bind(this);
        this.handleHideModal = this.handleHideModal.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            errors: null,
            isLoaded2: false,
            showModal: false,
            showToast: false,
            pokeName: "",
            pokeNickname: "",

        };
    }


    componentDidMount() {
        console.log("resListPoke", resListPoke)
        // console.log("resListPoke url", resListPoke.dataPoke)
        let datas = resListPoke.dataPoke
        let a = datas.url
        fetch(a)
            .then((res2) => { return res2.json() })
            .then((data2) => {
                // console.log("DATA 22", data2)
                let obj = {}
                obj.name = data2.name
                obj.img = data2.sprites.front_default
                obj.type = data2.types
                obj.move = data2.moves
                this.detailPokemon.push(obj)
                // console.log("obj", obj)
                console.log("pok", this.detailPokemon)
                this.setState({
                    isLoaded2: true,
                });
            },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                });
    }

    onSubmit() {

        let datas = {
            "nickname": this.state.pokeNickname,
            "datas": this.detailPokemon[0]
        }
        // console.log(this.state.pokeNickname)
        // console.log("isi_store", resListPoke.myPoke)
        let new_isi_store = resListPoke.myPoke.some(x =>
            x.nickname === this.state.pokeNickname
        )
        // console.log("new_isi_store", new_isi_store)
        if (new_isi_store) {
            alert("nickname is duplicate")
        } else {
            store.dispatch({ type: 'simpan', fill: datas })

        }
        this.setState({ showModal: false });
    };


    catchHandler(datas) {
        var a = Math.floor(Math.random() * 2)
        // var a = 1
        console.log(a)
        if (a === 1) {
            this.setState({ pokeName: datas.name })
            this.setState({ showModal: true })
        } else {
            this.setState({ showToast: true })
        }
    }
    handleHideToast() {
        this.setState({ showToast: false });
    }
    handleHideModal() {
        this.setState({ showModal: false });
    }

    render() {
        const { error, isLoaded2 } = this.state;
        if (error) {
            return <div className="error">Error: {error.message}</div>;
        } else if (!isLoaded2) {
            return <div className="loading">Loading...</div>;
        } else {
            return (
                <Container fluid>
                    <Modal
                        show={this.state.showModal}
                        onHide={this.handleHideModal}
                        aria-labelledby="contained-modal-title">
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                                YESS YOU'VE CAUGHT: {this.state.pokeName}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group className="m-0">
                                <Form.Label>Nickname</Form.Label>
                                <Form.Control
                                    className="textFeedback"
                                    as="textarea"
                                    placeholder="enter nickname"
                                    value={this.state.val}
                                    onChange={e => this.setState({ pokeNickname: e.target.value }
                                    )}
                                    type="text"
                                />
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                className="btnFormSend"
                                variant="success"
                                onClick={this.onSubmit}>
                                Keep
                                </Button>
                            <Button variant="outline-danger" onClick={this.handleHideModal}>Release</Button>
                        </Modal.Footer>
                    </Modal>
                    <Row>
                        {this.detailPokemon.map(pokemun => (
                            <Card className="kartu" style={{ width: '100%' }}>
                                <div className="parent">
                                    <Col md="auto"
                                        aria-live="polite"
                                        aria-atomic="true"
                                        style={{
                                            position: 'relative',
                                            minHeight: '100px',
                                        }}>
                                        <Toast style={{
                                            position: 'absolute',
                                            top: 0,
                                            right: 0,
                                            width: '160px'
                                        }}
                                            onClose={this.handleHideToast} show={this.state.showToast} delay={2000} autohide>
                                            <Toast.Header>
                                                <strong className="mr-auto">huhuu too bad, try again!!</strong>
                                            </Toast.Header>
                                        </Toast>
                                    </Col>
                                    <Card.Img variant="top" className="gambar-pokem" src={pokemun.img} />
                                </div>
                                <Card.Body>
                                    <Card.Title className="span-name">{pokemun.name}</Card.Title>
                                    <Row>
                                        <Card.Text>
                                            Type:
                                        </Card.Text>
                                        {pokemun.type.map(tipe => (
                                            <Col md="auto">
                                                <Card.Text>
                                                    {tipe.type.name}
                                                </Card.Text>
                                            </Col>

                                        ))}
                                    </Row>
                                    <Row>
                                        <Card.Text>
                                            Moves:
                                        </Card.Text>
                                        {pokemun.move.slice(0, 10).map(jurus => (
                                            <Col md="auto">
                                                <Card.Text>{jurus.move.name}</Card.Text>
                                            </Col>
                                        ))}
                                    </Row>
                                    <div className="parent2">
                                        <Button onClick={(e) => this.catchHandler(pokemun, e)}>
                                        <Image src={logo2} roundedCircle className="pokeball" /> Catch {pokemun.name} now!!
                                        </Button>
                                    </div>
                                </Card.Body>
                            </Card>
                        ))}
                    </Row>
                </Container>



            );
        }
    }

}