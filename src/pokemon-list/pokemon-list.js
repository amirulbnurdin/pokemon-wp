import React from 'react';
import './pokemon-list.css';
import { createStore } from 'redux'
import { pokeListStore } from '../store/redux.store';
// import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import { Card, Col, Container, Row } from 'react-bootstrap';

const store = createStore(pokeListStore)
export default class PokemonListClass extends React.Component {
  listPokemon = [];

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      pokemonDatas: []
    };
  }

  componentDidMount() {

    fetch(`https://pokeapi.co/api/v2/pokemon?limit=51`)
      .then((res) => { return res.json() })
      .then((data) => {
        console.log("BALIKAN API", data)
        this.listPokemon = data.results
        this.setState({
          isLoaded: true,
          items: data.results
        });

      },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        });
  }

  lihat(datas) {
    // console.log("datas", datas)
    store.dispatch({ type: 'isi', fill: datas })
    this.props.history.push('/detail/' + datas.name)
  }


  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <Container fluid>
          <Row>
            {this.listPokemon.map(pokemun => (
              <Col className="wrapper-kartu">
                <Card className="card-parent">
                  <Card.Img variant="top" src="" />
                  <Card.Body>
                    <Card.Title className="span-name">{pokemun.name}</Card.Title>
                    <Card.Text>
                      Some quick example text to build on the card title and make up the bulk of
                      the card's content.
                    </Card.Text>
                    <Button variant="info" onClick={(e) => this.lihat(pokemun, e)}> <h5>See Pokemon info</h5></Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      );
    }
  }

}

