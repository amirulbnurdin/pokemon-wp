import logo from './logo.svg';
import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import PokemonListClass from './pokemon-list/pokemon-list';
import PokemonDetailClass from './pokemon-detail/pokemon-detail';
import { Container } from '@material-ui/core';
import { Col, Nav, Navbar } from 'react-bootstrap';
import MyPokemonClass from './my-pokemon/my-pokemon';

function App() {

  return (
    <Container fluid>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Router>

        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">
            <img
              alt=""
              src={logo}
              className="d-inline-block align-top logo-kecil"
            />{' '}
              Pokemon WP
            </Navbar.Brand>
          <Nav className="pisah">
            <Col md="auto">
              <Link className="menu" to="/pokemon_list">Pokemon List</Link>
              <Link className="menu" to="/my_pokemon">My Pokemon</Link>
            </Col>
            <Col md="auto">
              <Nav.Link className="repo" href="https://gitlab.com/amirulbnurdin/pokemon-wp" target="_blank">Please kindly review my repo here</Nav.Link>
            </Col>
          </Nav>
        </Navbar>
        <Switch>
          <Route path="/pokemon_list" component={PokemonListClass}>
          </Route>
          <Route path="/detail/:detailId" component={PokemonDetailClass}>
          </Route>
          <Route path="/my_pokemon" component={MyPokemonClass}>
          </Route>
        </Switch>


      </Router>
    </Container>
  );
}

export default App;
