import React, { useEffect, useState } from 'react';
import './my-pokemon.css';
import { applyMiddleware, createStore } from 'redux'
import { pokeListStore, resListPoke } from '../store/redux.store';
import Button from 'react-bootstrap/Button';
import { Card, Col, Container, Row } from 'react-bootstrap';
import thunk from 'redux-thunk';

const store = createStore(pokeListStore, applyMiddleware(thunk))
export default function MyPokemonClass() {
    const [count, setCount] = useState(0);
    const listPokemon = resListPoke.myPoke;
    // console.log("resListPoke.myPoke", resListPoke.myPoke)

    useEffect(() => {

    });

    function lepas(datas) {
        let new_data = resListPoke.myPoke.filter(x =>
            x.nickname !== datas.nickname
        )
        // console.log("new_data", new_data)
        store.dispatch({ type: 'ganti', fill: new_data })
        setCount(count + 1)
    }

    return (

        <Container fluid>
            <Row>
                {listPokemon.map(pokemun => (
                    <Col md="auto" className="wrapper-kartu">
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <div className="row parent2">
                                    <Card.Title className="span-name">{pokemun.nickname}</Card.Title>
                                    <Card.Img variant="top" className="gambar-pokem" src={pokemun.datas.img} />
                                </div>
                                <Row>
                                    <Card.Text>
                                        Type:
                                        </Card.Text>
                                    {pokemun.datas.type.map(tipe => (
                                        <Col md="auto">
                                            <Card.Text>
                                                {tipe.type.name}
                                            </Card.Text>
                                        </Col>

                                    ))}
                                </Row>
                                <Row>
                                    <Card.Text>
                                        Moves:
                                            </Card.Text>
                                </Row>
                                <Row>
                                    {pokemun.datas.move.slice(0, 10).map(jurus => (
                                        <Col md="auto">
                                            <Card.Text>{jurus.move.name}</Card.Text>
                                        </Col>
                                    ))}
                                </Row>
                                <Button variant="outline-danger" onClick={(e) => lepas(pokemun, e)}> <h5>Release Pokemon</h5></Button>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>
    )

}

